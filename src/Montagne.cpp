
#include "Montagne.h"
#include "Piece.h"

Montagne::Montagne()
    :Piece()
{

}

Montagne::Montagne(int _posX,int _posY,float _resistance)
    :Piece(_posX,_posY,_resistance)
{

}

Montagne::Montagne(int _posX,int _posY)
    :Piece(_posX,_posY,0.9)
{

}

Montagne::Montagne(int _posX,char _posYLettre)
    :Piece(_posX,0,0.9)
{
    int _posY = 0;
    if(_posYLettre=='A')_posY=1;
    if(_posYLettre=='B')_posY=2;
    if(_posYLettre=='C')_posY=3;
    if(_posYLettre=='D')_posY=4;
    if(_posYLettre=='E')_posY=5;
    setPosY(_posY);
}

Montagne::~Montagne()
{

}
