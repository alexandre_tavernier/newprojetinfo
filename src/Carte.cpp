#include <fstream>
#include <streambuf>
#include "Carte.h"
#include "Animal.h"
#include "Montagne.h"


using namespace std;

Carte::Carte()
    :m_chemin("map.txt")
{

}

Carte::Carte(std::string _chemin)
    :m_chemin(_chemin)
{

}

void Carte::setChemin(std::string _chemin){m_chemin=_chemin;};

std::string Carte::getChemin() const{return m_chemin;};

bool Carte::afficherCarte()
{
    bool validation = true;
    std::string path = getChemin();
    std::ifstream ifs(path.c_str());
    if (ifs)
    {
        std::string line;
        while (std::getline(ifs, line))
        {
            std::cout << line << std::endl;
        }
        ifs.close();
    }
    else
    {
        validation = false;
    }
    return validation;

}
void Carte::remplacerCarte()
{
    fstream carteOriginale;
    carteOriginale.open("mapOriginale.txt",fstream::binary | fstream::out | fstream::in);
    fstream carte;
    std::string path = getChemin();
    carte.open(path.c_str(),fstream::binary | fstream::out | fstream::in);
    carte.flush();
    std::string str((std::istreambuf_iterator<char>(carteOriginale)),
                 std::istreambuf_iterator<char>());
    carte<<str<<endl;
    carteOriginale.close();
    carte.close();
}

char Carte::lireLettre(int x,int y,bool dir){
    char c;
    std::string s;
    fstream carte;
    std::string path = getChemin();
    carte.open(path.c_str(),fstream::binary | fstream::out | fstream::in);
    for(int i = 0;i<(y-1)*2+2;i++)
    getline(carte,s);
    if(x>0){
        if(!dir){
            c=s[5+(x-1)*3];
        }else{
            c=s[6+(x-1)*3];
        }
    }
    if(x==-1){
        c=s[20];
    }
    if(x==-2){
        c=s[1];
    }
return c;
}

void Carte::placeLettre(char s,int x,int y,bool dir){
   fstream carte;
   std::string path = getChemin();
   carte.open(path.c_str(),fstream::binary | fstream::out | fstream::in);
   int offset = 0;
    if(x>0){
        if(!dir){
            offset=23+(y-1)*25+(x-1)*3;
        }else{
            offset=23+(y-1)*25+(x-1)*3+1;
        }
    }
    if(x==-1){
        offset=38+(y-1)*25;
    }
    if(x==-2){
        offset=19+(y-1)*25;
    }
   carte.seekp(offset);
   carte.write(&s,1);
   carte.close();
}

bool Carte::modifierExterieur(char c,int i){
    int type;
    bool validation = true;
    if(c=='E'){
        type=-2;
    }else if(c=='R'){
        type=-1;
    }
    int y = 1;
    while((lireLettre(type,y+1,false)==c)&&(y<5)){
        y++;
    }
    if(i==1){
        if(lireLettre(type,5,false)==' '){
        placeLettre(c,type,y+1,false);
        }else{
            validation = false;
        }
    }
    if(i==-1){
        if(lireLettre(type,1,false)==c){
            placeLettre(' ',type,y,false);
        }else{
            validation = false;
        }
    }
    return validation;
}

bool Carte::ajouterAnimal(Animal *a){
    bool validation = true;
    if(lireLettre(a->getPosX(),a->getPosY(),false)!='.')validation=false;
    if(validation){
        placeLettre(a->getType(),a->getPosX(),a->getPosY(),false);
        placeLettre(a->getDirection(),a->getPosX(),a->getPosY(),true);
    }
    return validation;
}

bool Carte::enleverAnimal(Animal *a){
    bool validation = true;
    if(lireLettre(a->getPosX(),a->getPosY(),false)=='.')validation=false;
    if(validation){
        placeLettre('.',a->getPosX(),a->getPosY(),false);
        placeLettre(' ',a->getPosX(),a->getPosY(),true);
    }
    return validation;
}

bool Carte::ajouterMontagne(Montagne *m){
    bool validation = true;
    if(lireLettre(m->getPosX(),m->getPosY(),false)!='.')validation=false;
    if(validation){
        placeLettre('M',m->getPosX(),m->getPosY(),false);
    }

    return validation;
}

bool Carte::enleverMontagne(Montagne *m){
    bool validation = true;
    if(lireLettre(m->getPosX(),m->getPosY(),false)=='.')validation=false;
    if(validation){
        placeLettre('.',m->getPosX(),m->getPosY(),false);
        placeLettre(' ',m->getPosX(),m->getPosY(),true);
    }
    return validation;
}

Carte::~Carte()
{

}
