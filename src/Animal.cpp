#include "Animal.h"
#include "Carte.h"
#include <stdlib.h>
#include <iostream>


Animal::Animal()
    :m_type('0'),m_direction('0'),m_force(1)
{

}
Animal::Animal(char _type,char _direction,int _force,int _posX,int _posY,float _resistance)
        :Piece(_posX,_posY,_resistance),m_type(_type),m_direction(_direction),m_force(_force)
{

}

Animal::Animal(char _type,char _direction,int _posX,char _posYLettre)
        :Piece(_posX,0,1),m_type(_type),m_direction(_direction),m_force(1)
{
    int _posY = 0;
    if(_posYLettre=='A')_posY=1;
    if(_posYLettre=='B')_posY=2;
    if(_posYLettre=='C')_posY=3;
    if(_posYLettre=='D')_posY=4;
    if(_posYLettre=='E')_posY=5;
    setPosY(_posY);
}


void Animal::setType(char _type){m_type=_type;};

void Animal::setDirection(char _direction){m_direction=_direction;};

char Animal::getType() const{return m_type;};

char Animal::getDirection() const{return m_direction;};

int Animal::getForce() const{return m_force;};

bool Animal::ajouterAnimal(Carte *_carte){
    bool validation = true;
    if(_carte->lireLettre(getPosX(),getPosY(),false)!='.')validation=false;
    if(!_carte->modifierExterieur(getType(),-1))validation=false;
    //if(!validation)_carte->modifierExterieur(a->getType(),1);
    if(((getPosX()!=1)&&(getPosX()!=5))&&((getPosY()!=1)&&(getPosY()!=5)))validation=false;
    if(validation){
        _carte->placeLettre(getType(),getPosX(),getPosY(),false);
        _carte->placeLettre(getDirection(),getPosX(),getPosY(),true);
    }
    return validation;

}

bool Animal::bougerAnimal(char _direction,Carte *_carte)
{

    bool validation = true;
    int deplacementX;
    int deplacementY;
    if(_direction=='h'){deplacementX=0;deplacementY=-1;};
    if(_direction=='d'){deplacementX=1;deplacementY=0;};
    if(_direction=='b'){deplacementX=0;deplacementY=1;};
    if(_direction=='g'){deplacementX=-1;deplacementY=0;};
    if(((getPosX()+deplacementX)<=0)||((getPosX()+deplacementX)>=6))validation=false;
    if(((getPosY()+deplacementY)<=0)||((getPosY()+deplacementY)>=6))validation=false;
    if(validation){
        if(_carte->lireLettre(getPosX()+deplacementX,getPosY()+deplacementY,false)!='.')validation=false;
    };
    if(validation){
        _carte->enleverAnimal(this);
        setPosX(getPosX()+deplacementX);
        setPosY(getPosY()+deplacementY);
        _carte->ajouterAnimal(this);
    }

    return validation;
}

bool Animal::changerDirection(char _direction,Carte *_carte)
{
    bool validation = true;
    if((_carte->lireLettre(getPosX(),getPosX(),false)!=getType())||(_carte->lireLettre(getPosX(),getPosX(),true)!=getDirection()))validation=false;
    if(validation){
        setDirection(_direction);
        _carte->placeLettre(getDirection(),getPosX(),getPosY(),true);
    }
    return validation;
}

bool Animal::sortirAnimal(Carte *_carte)
{
    bool validation = true;
    if(((getPosX()!=1)&&(getPosX()!=5))&&((getPosY()!=1)&&(getPosY()!=5)))validation=false;
    if(validation){
        if(!_carte->enleverAnimal(this))validation=false;
        if(validation){
            if(!_carte->modifierExterieur(getType(),1))validation=false;
        }
    }
    return validation;
}

bool Animal::pousser(char _direction,Carte *_carte)
{
    bool validation = true;
    int debut,incrementation,fin;
    bool axeX,axeY;
    char directionInverse;
    if(_direction=='h'){
        axeY=true;axeX=false;
        debut=getPosY();
        incrementation=-1;
        fin=1;
        directionInverse='b';
    }
    if(_direction=='b'){
        axeY=true;axeX=false;
        debut=getPosY();
        incrementation=1;
        fin=5;
        directionInverse='h';
    }
    if(_direction=='d'){
        axeY=false;axeX=true;
        debut=getPosX();
        incrementation=1;
        fin=5;
        directionInverse='g';
    }
    if(_direction=='g'){
        axeY=false;axeX=true;
        debut=getPosY();
        incrementation=-1;
        fin=1;
        directionInverse='d';
    }

    bool caseLibre=false;
    float resistanceTotale = 1;

    int i = debut;
    while((i<fin)&&(!caseLibre)){
        i=i+incrementation;
        if(axeX){
            if(_carte->lireLettre(i,getPosY(),false)!='.'){
                if((_carte->lireLettre(i,getPosY(),true)==directionInverse))resistanceTotale=resistanceTotale-1;
                if((_carte->lireLettre(i,getPosY(),true)==_direction)&&_carte->lireLettre(i+incrementation,getPosY(),false)!='.')resistanceTotale=resistanceTotale+1;
                if((_carte->lireLettre(i,getPosY(),false)=='M'))resistanceTotale=resistanceTotale-0.9;
            }else{
                caseLibre=true;

            }
        }
        if(axeY){
            if(_carte->lireLettre(getPosX(),i,false)!='.'){
                if((_carte->lireLettre(getPosX(),i,true)==directionInverse))resistanceTotale=resistanceTotale-1;
                if((_carte->lireLettre(getPosX(),i,true)==_direction)&&_carte->lireLettre(getPosX(),i+incrementation,false)!='.')resistanceTotale=resistanceTotale+1;
                if((_carte->lireLettre(getPosX(),i,false)=='M'))resistanceTotale=resistanceTotale-0.9;
            }else{
                caseLibre=true;
            }
        }
    }
    if(resistanceTotale<=0){
        validation=false;
    }
    if(validation){

    }
    return validation;
}


Animal::~Animal()
{

}
