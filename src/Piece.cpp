#include "Piece.h"
#include "Carte.h"

Piece::Piece()
    : m_posX(0),m_posY(0),m_resistance(0)
{
    //ctor
}

Piece::Piece(int _posX,int _posY,float _resistance)
    : m_posX(_posX),m_posY(_posY),m_resistance(_resistance)
{

}
void Piece::setPosX(int _posX){m_posX=_posX;};

void Piece::setPosY(int _posY){m_posY=_posY;};

void Piece::setResistance(float _resistance){m_resistance=_resistance;};

int Piece::getPosX() const{return m_posX;};

int Piece::getPosY() const{return m_posY;};

float Piece::getResistance() const{return m_resistance;};

Piece::~Piece()
{
    //dtor
}
