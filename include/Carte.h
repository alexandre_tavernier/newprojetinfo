#ifndef CARTE_H
#define CARTE_H
#include <iostream>
class Animal;
class Montagne;
using namespace std;

class Carte
{
    public:
        Carte();
        Carte(std::string _chemin);
        void setChemin(std::string _chemin);
        std::string getChemin() const;
        bool afficherCarte();
        void remplacerCarte();
        char lireLettre(int x,int y,bool dir);
        void placeLettre(char s,int x,int y,bool dir);
        bool modifierExterieur(char c,int i);
        bool ajouterAnimal(Animal *a);
        bool enleverAnimal(Animal *a);
        bool ajouterMontagne(Montagne *m);
        bool enleverMontagne(Montagne *m);
        virtual ~Carte();
    protected:
    private:
        std::string m_chemin;
};

#endif // CARTE_H
