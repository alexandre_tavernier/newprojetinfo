#ifndef ANIMAL_H
#define ANIMAL_H
#include <iostream>
#include "Piece.h"
#include "Carte.h"


class Animal : public Piece
{
    public:
        Animal();
        Animal(char _type,char _direction,int _force,int _posX,int _posY,float _resistance);
        Animal(char _type,char _direction,int _posX,char _posYLettre);
        void setType(char _type);
        void setDirection(char _direction);
        char getType() const;
        char getDirection() const;
        int getForce() const;
        bool ajouterAnimal(Carte *_carte);
        bool bougerAnimal(char _direction,Carte *_carte);
        bool changerDirection(char _direction,Carte *_carte);
        bool sortirAnimal(Carte *_carte);
        bool pousser(char _direction,Carte *_carte);
        virtual ~Animal();
    protected:
    private:
        char m_type;
        char m_direction;
        int m_force;
};

#endif // ANIMAL_H
