#ifndef PIECE_H
#define PIECE_H
#include "Carte.h"


class Piece
{
    public:
        Piece();
        Piece(int _posX,int _posY,float _resistance);
        void setPosX(int _posX);
        void setPosY(int _posY);
        void setResistance(float _resistance);
        int getPosX() const;
        int getPosY() const;
        float getResistance() const;
        virtual ~Piece();
    protected:
    private:
        int m_posX;
        int m_posY;
        float m_resistance;
};

#endif // PIECE_H
