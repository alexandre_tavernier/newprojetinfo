#ifndef MONTAGNE_H
#define MONTAGNE_H
#include <iostream>
#include "Piece.h"


class Montagne : public Piece
{
    public:
        Montagne();
        Montagne(int _posX,int _posY,float _resistance);
        Montagne(int _posX,int _posY);
        Montagne(int _posX,char _posYLettre);
        virtual ~Montagne();
    protected:
    private:
};


#endif // MONTAGNE_H
