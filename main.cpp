#include <iostream>
#include<cstdio>
#include <fstream>
#include <string>
#include <fstream>
#include <streambuf>
#include "Animal.h"
#include "Piece.h"
#include "Carte.h"
#include "Montagne.h"

using namespace std;


int main()
{
    Carte *c = new Carte("map.txt");
    c->remplacerCarte();
    Montagne *m1 = new Montagne(3,'B');
    Montagne *m2 = new Montagne(3,'C');
    Montagne *m3 = new Montagne(3,'D');

    cout<<c->ajouterMontagne(m1);
    cout<<c->ajouterMontagne(m2);
    cout<<c->ajouterMontagne(m3)<<endl;

    Animal *a1 = new Animal('R','h',1,'A');
    Animal *a2 = new Animal('E','b',1,'B');
    Animal *a3 = new Animal('E','d',2,'A');
    Animal *a4 = new Animal('R','g',5,'E');
    Animal *a5 = new Animal('R','g',5,'B');

    c->afficherCarte();
    cout<<a1->ajouterAnimal(c);
    cout<<a2->ajouterAnimal(c);
    cout<<a3->ajouterAnimal(c);
    cout<<a5->ajouterAnimal(c);
    cout<<a4->ajouterAnimal(c)<<endl;
    c->afficherCarte();
    cout<<a2->bougerAnimal('b',c);
    cout<<a4->bougerAnimal('b',c);
    cout<<a4->changerDirection('h',c);
    cout<<a3->bougerAnimal('d',c);
    cout<<a1->bougerAnimal('b',c);
    cout<<a5->bougerAnimal('g',c);
    cout<<a1->bougerAnimal('d',c)<<endl;
    c->afficherCarte();
    cout<<a1->sortirAnimal(c);
    cout<<a2->sortirAnimal(c);
    cout<<a3->sortirAnimal(c);
    cout<<a4->sortirAnimal(c)<<endl;
    c->afficherCarte();
    cout<<a1->pousser('d',c)<<endl;
    c->afficherCarte();
    c->remplacerCarte();
    return 0;
}
